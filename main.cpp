#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include <QtQml/QQmlContext>

#include <source/hostinfo.h>
#include <source/networkdiscovery.h>
#include <source/countdown.h>
#include <source/tcpconnect.h>

// Utils
#include <source/utils/statusbar/statusbar.h>
#include <source/utils/share/share.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Marssola");
    QCoreApplication::setApplicationName("Timer IN LAN");
    QCoreApplication::setApplicationVersion("1.0.1");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/img/Timer.png"));

    qmlRegisterType<HostInfo>("HostInfo", 0, 1, "HostInfo");
    qmlRegisterType<NetworkDiscovery>("NetworkDiscovery", 0, 1, "NetworkDiscovery");
    qmlRegisterType<TcpConnect>("TcpConnect", 0, 1, "TcpConnect");
    qmlRegisterType<Countdown>("Countdown", 0, 1, "Countdown");

    // Utils
    qmlRegisterType<StatusBar>("StatusBar", 0, 1, "StatusBar");
    qmlRegisterType<Share>("Share", 0, 1, "Share");

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("QT_VERSION", QT_VERSION_STR);
    context->setContextProperty("GIT_VERSION", GIT_VERSION);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

package com.timer;

import org.qtproject.qt5.android.QtNative;

import java.lang.String;
import android.content.Intent;
import android.util.Log;

public class QShareUtils
{
    protected QShareUtils()
    {
    }

    public static void share(String content)
    {
        if (QtNative.activity() == null)
            return;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, content);
        sendIntent.setType("text/plain");
        QtNative.activity().startActivity(sendIntent);
    }
}

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.LocalStorage 2.0

import "../js/Database.js" as DB
import "./components"

Dialog {
    id: dialog
    title: qsTr("Adicionar Tempo")

    parent: ApplicationWindow.overlay
    width: window.width < 400? window.width -10 : 400
    height: window.height > 350? 350 : window.height - window.header.height - 10

    modal: true
    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    padding: 5
    clip: true

    closePolicy: Popup.NoAutoClose

    contentItem: Flickable {
        width: parent.width
        height: parent.height
        contentHeight: column.implicitHeight

        Column {
            id: column
            width: parent.width
            anchors.margins: 5

            RowLayout {
                width: parent.width - 10
                height: 60

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true
                    text: "\uE91F"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TextField {
                    id: add_discourse_name
                    Layout.fillWidth: true
                }
            }

            RowLayout {
                width: parent.width
                height: 70

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true

                    text: "\uE924"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TimerComponent {
                    id: add_timer
                    itemWidth: 200
                    Layout.fillHeight: true
                    textSize: 16
                }
            }

            RowLayout {
                width: parent.width
                height: 70

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true
                    text: "\uE7F7"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TimerComponent {
                    id: add_alert
                    itemWidth: 200
                    Layout.fillHeight: true
                    textSize: 16
                }
            }
        }
    }

    footer: DialogButtonBox {
        Button {
            text: qsTr("Cancel")
            flat: true

            onClicked: {
                dialog.close()
            }
        }
        Button {
            text: qsTr("Add")
            flat: true

            onClicked: {
                var error = false
                var m_message = []
                if (add_discourse_name.text === "")
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o Discurso/Orador"))
                    add_discourse_name.focus = true
                }

                if (add_timer.getHours == 0 && add_timer.getMinutes == 0 && add_timer.getSeconds == 0)
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o tempo"))
                }

                if (add_alert.getHours == 0 && add_alert.getMinutes == 0 && add_alert.getSeconds == 0)
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o tempo de alerta"))
                }

                if (error)
                {
                    object.notification = m_message.join("<br/>")
                    return;
                }

                DB.setDiscourseList([
                    add_discourse_name.text,
                    new Date("1970-01-01 "+completeZero(add_timer.getHours)+":"+completeZero(add_timer.getMinutes)+":"+completeZero(add_timer.getSeconds)+" -0000").getTime() /1000,
                    new Date("1970-01-01 "+completeZero(add_alert.getHours)+":"+completeZero(add_alert.getMinutes)+":"+completeZero(add_alert.getSeconds)+" -0000").getTime() /1000,
                    0,
                    list_defined_time.count
                ]);
                add_discourse_name.text = ""
                add_timer.setHours.currentIndex = 0
                add_timer.setMinutes.currentIndex = 0
                add_timer.setSeconds.currentIndex = 0

                getListDefinedTime();
                dialog.close()
            }
        }
    }
}

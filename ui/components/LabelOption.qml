import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Label {
    id: control
    property bool borderBottom: true

    width: parent.width
    wrapMode: Label.WordWrap

    color: Qt.rgba(Material.foreground.r, Material.foreground.g, Material.foreground.b, 0.7)

    bottomPadding: borderBottom? 5 : 0

    Column {
        visible: control.borderBottom
        width: parent.width
        anchors.bottom: parent.bottom
        opacity: 0.5

        Rectangle {
            width: parent.width
            height: 1

            color: object.border_up
        }
        Rectangle {
            width: parent.width
            height: 1

            color: object.border_down
        }
    }
}

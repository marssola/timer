import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Label {
    width: parent.width
    height: 28

    font.capitalization: Font.AllUppercase
    opacity: 0.6

    anchors.left: parent.left
    anchors.leftMargin: 5

    Rectangle {
        width: parent.width
        height: 1
        color: object.border_up
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 2
    }
    Rectangle {
        width: parent.width
        height: 1
        color: object.border_down
        anchors.bottom: parent.bottom
    }
}

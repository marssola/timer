import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    id: control
    padding: 10

    property string icon
    property string iconName: material_icon.name
    property bool expandable: false
    property bool showContent: false

    label: RowLayout {
        id: row
        x: control.leftPadding
        width: control.availableWidth

        Label {
            text: control.icon
            font.family: control.iconName
            font.pixelSize: 25

            MouseArea {
                enabled: control.expandable
                anchors.fill: parent

                onClicked: control.showContent = ! control.showContent
            }
        }

        Label {
            Layout.fillWidth: true
            text: control.title
            elide: Text.ElideRight
            font.bold: true
            font.capitalization: Font.AllUppercase

            MouseArea {
                enabled: control.expandable
                anchors.fill: parent

                onClicked: control.showContent = ! control.showContent
            }
        }

        Label {
            visible: control.expandable
            text: "\uE5CF"
            font.family: material_icon.name
            font.pixelSize: 25

            rotation: control.showContent? 180 : 0

            Behavior on rotation {
                NumberAnimation {
                    duration: 150
                }
            }

            MouseArea {
                enabled: control.expandable
                anchors.fill: parent

                onClicked: control.showContent = ! control.showContent
            }
        }
    }

    background: Rectangle {
        y: control.topPadding - control.bottomPadding
        width: parent.width
        height: parent.height - control.topPadding + control.bottomPadding
        color: "transparent"

        Column {
            width: parent.width

            Rectangle {
                width: parent.width
                height: 1
                color: object.border_up
            }

            Rectangle {
                width: parent.width
                height: 1
                color: object.border_down
            }
        }

    }
}

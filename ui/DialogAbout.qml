import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

import "./components"

Dialog {
    id: dialog
    property bool mobile

    title: qsTr("Sobre")
    width: window.width < 300? window.width -10 : 300
    height: window.height < 450? window.height - window.header.height : 450

    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    modal: true
    clip: true
    padding: 1

    standardButtons: Dialog.Close
    closePolicy: Popup.NoAutoClose

    Flickable {
        anchors.fill: parent
        anchors.topMargin: mobile? header.height : 0
        anchors.bottomMargin: footer.height
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            anchors.margins: 5
            spacing: 2

            Image {
                width: 100
                fillMode: Image.PreserveAspectFit

                source: "qrc:/img/Timer.png"
                sourceSize.width: width
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                width: parent.width

                text: Qt.application.name
                font.pixelSize: 20
                font.bold: true
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                width: parent.width

                text: qsTr("Versão: ") + Qt.application.version
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                width: parent.width

                text: qsTr("Versão QT: ") + QT_VERSION
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                width: parent.width

                text: qsTr("Versão Revisão GIT: ") + GIT_VERSION
                horizontalAlignment: Label.AlignHCenter
            }

            Item {
                width: parent.width
                height: 4
            }

            Column {
                width: parent.width

                Rectangle {
                    width: parent.width
                    height: 1
                    color: object.border_up
                }

                Rectangle {
                    width: parent.width
                    height: 1
                    color: object.border_down
                }
            }

            Item {
                width: parent.width
                height: 4
            }

            Column {
                width: parent.width
                spacing: 2

                Label {
                    width: parent.width

                    text: qsTr("Desenvolvido por Mauro Marssola")
                    horizontalAlignment: Label.AlignHCenter
                }


                Label {
                    width: parent.width

                    text: "https://bitbucket.org/marssola/timer"
                    font.underline: true
                    color: Material.color(Material.Red)

                    horizontalAlignment: Label.AlignHCenter

                    MouseArea {
                        anchors.fill: parent

                        cursorShape: Qt.PointingHandCursor
                        onClicked: Qt.openUrlExternally("https://bitbucket.org/marssola/timer")
                    }
                }

                Item {
                    width: parent.width
                    height: 5
                }

                Label {
                    width: parent.width

                    text: qsTr("Copyright 2018. All rights reserved")
                    horizontalAlignment: Label.AlignHCenter
                }

                Label {
                    width: parent.width
                    text: qsTr("The app is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY FOR A PARTICULAR PURPOSE.")
                    horizontalAlignment: Label.AlignHCenter
                    font.pixelSize: 11
                    padding: 5
                    wrapMode: Label.WordWrap
                }
            }

            Image {
                height: 40
                fillMode: Image.PreserveAspectFit
                source: "qrc:/img/built_with_qt.png"
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }

    onClosed: deleteDialog()
}

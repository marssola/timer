import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

import "./components"

Dialog {
    id: dialog
    property bool mobile

    property int margin: 20
    property int pixelSizeValue: 20

    title: settings.type? qsTr("Dispositivos conectados") : qsTr("Conectar ao Controlador")
    width: window.width < 350? window.width -10 : 350
    height: window.height > 400? (settings.type? 400 : 300) : window.height - window.header.height - 10

    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    modal: true
    clip: true
    padding: 1

    standardButtons: Dialog.Close
    closePolicy: Popup.NoAutoClose

    Flickable {
        anchors.fill: parent
        anchors.topMargin: mobile? header.height : 0
        anchors.bottomMargin: footer.height
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            height: dialog.height - dialog.header.height - dialog.footer.height - dialog.margins

            anchors.margins: 5

            Column {
                visible: !settings.type
                width: parent.width

                ItemDelegate {
                    width: parent.width
                    enabled: !tcp_connect.receiver_connect

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.preferredWidth: 80
                            Layout.leftMargin: 20

                            text: qsTr("Controlador")
                            font.bold: true
                        }

                        Label {
                            Layout.fillWidth: true
                            Layout.rightMargin: 20
                            width: parent.width

                            text: (settings.controller_addr === "")? qsTr("Nenhum controlador definido") :
                                                                     (settings.controller_name)? settings.controller_name + " ("+ settings.controller_addr +")" :
                                                                                                 settings.controller_addr
                            horizontalAlignment: Label.AlignRight
                            elide: Label.ElideRight
                        }
                    }

                    onClicked: loadDialog("NetworkDiscovery")
                }

                ItemDelegate {
                    width: parent.width
                    enabled: !tcp_connect.receiver_connect

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.preferredWidth: 80
                            Layout.leftMargin: 20

                            text: qsTr("PIN")
                            font.bold: true
                        }

                        TextField {
                            id: field_pin

                            Layout.fillWidth: true
                            Layout.rightMargin: 20
                            width: parent.width

                            placeholderText: qsTr("Código PIN")
                            text: settings.controller_pin
                            horizontalAlignment: TextField.AlignRight
                            onTextChanged: (text === "")? settings.controller_pin = parseInt(0) : settings.controller_pin = parseInt(text)

                            onPressed: text = ""
                        }
                    }
                }

                Column {
                    width: parent.width
                    leftPadding: dialog.margin
                    rightPadding: dialog.margin

                    Button {
                        text: tcp_connect.receiver_connect? qsTr("Desconectar") : qsTr("Conectar")
                        width: parent.width - dialog.margin * 2
                        Material.background: tcp_connect.receiver_connect? Material.color(Material.Red, Material.Shade500) :
                                                                           Material.color(Material.Green, Material.Shade500)
                        onClicked: validateFields();
                    }
                }
            }

            ColumnLayout {
                visible: settings.type
                width: parent.width
                height: parent.height

                Button {
                    Layout.fillWidth: true
                    text: qsTr("Enviar sinal de conexão")
                    Material.background: Material.color(Material.Green, Material.Shade500)

                    onClicked: {
                        networkDiscovery.sendSignalToConnection()
                        object.notification = qsTr("Sinal de conexão enviado, aguarde...")
                    }
                }

                Label {
                    visible: !tcp_connect.devices

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    text: qsTr("Nenhum dispositivo conectado...")
                    verticalAlignment: Label.AlignVCenter
                    horizontalAlignment: Label.AlignHCenter
                    opacity: 0.7

                    SequentialAnimation on opacity {
                        running: tcp_connect.devices? false : true

                        OpacityAnimator {
                            from: 0.7
                            to: 0
                            duration: 1000
                            loops: Animation.Infinite
                        }
                        OpacityAnimator {
                            from: 0
                            to: 0.7
                            duration: 1000
                            loops: Animation.Infinite
                        }
                    }
                }

                ListView {
                    visible: tcp_connect.devices
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true

                    model: tcp_connect.list_devices
                    delegate: SwipeDelegate {
                        id: swipeDelegate

                        width: parent.width
                        text: modelData.name !== ""? modelData.name + " (" + modelData.ip + ")" : modelData.ip

                        swipe.right: Label {
                            id: deleteLabel

                            text: "\uE15C"
                            font.family: material_icon.name
                            font.pixelSize: 20
                            color: Material.color(Material.Red)

                            anchors.right: parent.right
                            height: parent.height
                            width: height
                            verticalAlignment: Qt.AlignVCenter
                            padding: 15

                            background: Rectangle {
                                color: mousearea_swipedelegate.pressed ? Qt.darker(Material.background, 1.1) : Qt.darker(Material.background, 0.8)

                                MouseArea {
                                    id: mousearea_swipedelegate
                                    anchors.fill: parent
                                    onClicked: tcp_connect.server_disconnectClient(modelData.ip)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: loader
        asynchronous: true
        active: false
    }

    function loadDialog(page, parameters)
    {
        if (parameters === undefined)
            parameters = { visible: true, mobile: object.mobile }
        else
        {
            parameters.visible = true
            parameters.mobile = object.mobile
        }

        loader.setSource("Dialog" + page + ".qml", parameters)
        loader.active = true
    }

    onClosed: deleteDialog()
}

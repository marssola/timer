import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

Item {
    id: item
    property alias running_timer: running_timer

    anchors.fill: parent

    ColumnLayout {
        anchors.fill: parent

        Rectangle {
            color: "transparent"
            Layout.fillHeight: true
            Layout.fillWidth: true

            TimerComponent {
                id: running_timer
                anchors.fill: parent
                blink: countdown.time_closed

                MouseArea {
                    anchors.fill: parent
                }
            }

            SequentialAnimation on color {
                running: object.alert

                ColorAnimation {
                    to: object.alert_color
                    duration: object.alert
                }
            }

            SequentialAnimation on color {
                running: !object.alert

                ColorAnimation {
                    to: "transparent"
                    duration: 5000
                }
            }
        }
    }
}

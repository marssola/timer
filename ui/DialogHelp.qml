import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

import "./components"

Dialog {
    id: dialog
    property bool mobile

    title: qsTr("Ajuda")
    width: window.width < 600? window.width -10 : 600
    height: window.height > 600? 600 : window.height - window.header.height - 10

    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    modal: true
    clip: true
    padding: 1

    standardButtons: Dialog.Close
    closePolicy: Popup.NoAutoClose

    Flickable {
        anchors.fill: parent
        anchors.topMargin: mobile? header.height : 0
        anchors.bottomMargin: footer.height
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            anchors.margins: 10
            spacing: 5

            GroupBox {
                title: Qt.application.name
                width: parent.width

                LabelOption {
                    text: qsTr("É um aplicativo de cronômetro que compartilha o tempo por meio de uma rede local (LAN).")
                    borderBottom: false
                }
            }

            GroupBox {
                title: qsTr("Como funciona?")
                width: parent.width

                LabelOption {
                    text: qsTr("Todos os dispositivos precisam estar na mesma rede, e um deles precisa ser configurado como Controlador, os demais, como Receptores.")
                    borderBottom: false
                }
            }

            GroupBox {
                title: qsTr("Controlador")
                width: parent.width

                LabelOption {
                    text: qsTr("É o dispositivo principal que determinará as configurações para todos os outros.<br/>Com o Controlador, é possível criar uma lista de discursos/apresentações com seu respectivo tempo e alerta de término.")
                    borderBottom: false
                }
            }

            GroupBox {
                title: qsTr("Receptor")
                width: parent.width

                LabelOption {
                    text: qsTr("É o dispositivo que se conecta ao Controlador para receber os comandos e configurações. Exibe apenas o tempo definido pelo Controlador")
                    borderBottom: false
                }
            }

            GroupBox {
                id: group_settings
                title: qsTr("Configurações")
                icon: "\uE8B8"
                expandable: true
                width: parent.width

                Column {
                    width: parent.width
                    spacing: 10

                    Column {
                        visible: group_settings.showContent
                        width: parent.width
                        spacing: 5

                        LabelOption {
                            text: qsTr("Seção Dispositivo")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Nome")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Nome do dispositivo que será exibido na rede.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Endereço IP")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Lista de Endereços IP do dispositivo. Se houver mais de uma interface de rede, poderá conter mais de um IP - Toque para obter a lista completa.")
                            }
                        }


                        LabelOption {
                            text: qsTr("Seção App")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Controlador")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define se o dispositivo será um Controlador (Marcado) ou Receptor (Desmarcado).")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("PIN")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Código de segurança do Controlador para que os Receptores possam se conectar.")
                            }
                        }

                        LabelOption {
                            visible: !object.mobile
                            text: qsTr("Layout")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Otimizar para<br/>telas OLED")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define a cor de fundo do App para preto, economizando energia em telas OLED.")
                            }
                        }

                        LabelOption {
                            visible: !object.mobile
                            text: qsTr("Seção Tela/Janela")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            visible: !object.mobile
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Remover barra de título<br/>e botões da janela")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Maximiza o uso da tela por remover a barra de título e botões da janela.")
                            }
                        }

                        RowLayout {
                            visible: !object.mobile
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Tela cheia")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Maximiza o uso da tela por deixar o App em tela cheia (Full screen).")
                            }
                        }

                        LabelOption {
                            visible: !object.mobile
                            text: qsTr(" ")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Reconfigurar o App")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Volta para todas as configurações iniciais do App.")
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: group_devices
                title: qsTr("Dispositivos Conectados")
                icon: "\uE337"
                expandable: true
                width: parent.width

                Column {
                    width: parent.width
                    spacing: 10

                    Column {
                        visible: group_devices.showContent
                        width: parent.width
                        spacing: 5

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Enviar sinal<br/>de conexão")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Este botão envia um sinal de conexão na rede para que os receptores possam se conectar ao Controlador.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Lista de dispositivos conectados")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Dispositivo (Endereço IP)")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Nome do dispositivo Definido na tela de Configurações -> Dispositivo -> Nome (Endereço IP do receptor conectado).")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE15C"
                                font.family: material_icon.name
                                font.pixelSize: 20
                                color: Material.color(Material.Red)
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Botão para encerrar a conexão com o receptor.<br/>Toque em Dispositivo (Endereço IP) e deslize para a esquerda para acessar este botão.")
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: group_connect_controller
                title: qsTr("Conectar ao Controlador")
                icon: "\uE337"
                expandable: true
                width: parent.width

                Column {
                    width: parent.width
                    spacing: 10

                    Column {
                        visible: group_connect_controller.showContent
                        width: parent.width
                        spacing: 5

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Controlador")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Nome do Dispositivo e Endereço IP do Controlador que o Receptor irá estabelecer conexão.<br/>Para descobrir o dispositivo Controlador na rede, toque neste item.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Procurar Controlador")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Configuração<br/>manual")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Se habilitado, será exibido um campo de texto para inserir o Endereço IP do Controlador.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE8B6"
                                font.family: material_icon.name
                                font.pixelSize: 15
                                Material.background: Material.color(Material.Green, Material.Shade500)
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Botão que enviará um sinal para a rede em busca de Controladores.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Procurar Controlador -> Lista de dispositivos encontrados")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Dispositivo (Endereço IP)")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Nome do dispositivo (Endereço IP) do Controlador encontrado.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("OK")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define o Dispositivo Controlador que serå usado para se estabelecer uma conexão.<br/>Certifique-se de escolher na Lista de dispositivos encontrados um Controlador.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Cancelar")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Não define o Controlador selecioda/inserido.")
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: group_list_time
                title: qsTr("Lista de Tempo")
                icon: "\uE05F"
                expandable: true
                width: parent.width

                Column {
                    width: parent.width
                    spacing: 10

                    Column {
                        visible: group_list_time.showContent
                        width: parent.width
                        spacing: 5

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: qsTr("Adicionar")
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Abre um Popup para adicionar um novo tempo.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Adicionar Tempo")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE91F"
                                font.family: material_icon.name
                                font.pixelSize: 30
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Nome do discurso/apresentação/orador.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE924"
                                font.family: material_icon.name
                                font.pixelSize: 30
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Tempo que será estabelecido para o cronômetro.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE7F7"
                                font.family: material_icon.name
                                font.pixelSize: 30
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Tempo de alerta de término para o cronômetro (Tempo antes do fim).")
                            }
                        }

                        LabelOption {
                            text: qsTr("Editar Tempo")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE422"
                                font.family: material_icon.name
                                font.pixelSize: 30
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Tempo decorrido.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                flat: true
                                text: "\uE8B3"
                                font.family: material_icon.name
                                font.pixelSize: 25
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Resetar o Tempo decorrido.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Lista de Tempo")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        LabelOption {
                            Layout.fillWidth: true
                            text: qsTr("Toque em qualquer item da lista para editar as configurações do Tempo.")
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE872"
                                font.family: material_icon.name
                                font.pixelSize: 20
                                color: Material.color(Material.Red)
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Botão para remover o item da lista de Tempo.<br/>Toque no item e deslize para a esquerda para acessar este botão.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "\uE80D"
                                font.family: material_icon.name
                                font.pixelSize: 20
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Botão para compartilhar o item.<br/>Toque no item e deslize para a direita para acessar este botão.<br/>*No desktop, a lista é enviada para área de transferência do sistema.")
                            }
                        }

                        LabelOption {
                            text: qsTr("Botões")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                flat: true
                                text: "\uE872"
                                font.family: material_icon.name
                                font.pixelSize: 25
                                Material.foreground: Material.Red
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Remove todos os itens da lista.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                flat: true
                                text: "\uE8B3"
                                font.family: material_icon.name
                                font.pixelSize: 25
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Zera o tempo decorrido de todos os itens da lista.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                flat: true
                                text: "\uE80D"
                                font.family: material_icon.name
                                font.pixelSize: 25
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Compartilha todos os itens da lista.<br/>*No desktop, a lista é enviada para área de transferência do sistema.")
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: group_controller_functions
                title: qsTr("Funções do Controlador")
                expandable: true
                width: parent.width

                Column {
                    width: parent.width

                    Column {
                        visible: group_controller_functions.showContent
                        width: parent.width
                        spacing: 5

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE044"
                                font.family: material_icon.name
                                font.pixelSize: 20
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define o próximo Tempo da lista disponível.<br/>Este botão só é exibido quando há ao menos um Tempo definido na lista.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE037"
                                font.family: material_icon.name
                                font.pixelSize: 20

                                Material.background: Material.color(Material.Green, Material.Shade500)
                                Material.elevation: 10
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Inicia a contagem do Tempo definido.<br/>Este botão só estará habilitado após o Tempo ter sido definido no passo anterior.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE034"
                                font.family: material_icon.name
                                font.pixelSize: 20

                                Material.background: Material.color(Material.Amber, Material.Shade500)
                                Material.elevation: 10
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Pausa o cronômetro.<br/>Para acionar este botão, é preciso tocar e segurar. Uma vez acionado, não será permitido o cronômetro continuar de onde parou.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE161"
                                font.family: material_icon.name
                                font.pixelSize: 20
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Encerra a contagem, salva o Tempo decorrido e zera as configurações.<br/>Enquanto a contagem estiver em andamento, para acionar este botão é necessário tocar e segurar. Se o tempo estiver pausado, é possível acionar com um toque simples.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                text: "\uE8B3"
                                font.family: material_icon.name
                                font.pixelSize: 20
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Reinicia as configurações do Tempo definido.<br/>Este botão é exibido quando o Tempo está Pausado.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            RoundButton {
                                flat: true
                                text: "\uE14A"
                                font.family: material_icon.name
                                font.pixelSize: 20
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Remove o Tempo definido.")
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: group_shortcuts
                title: qsTr("Teclas de atalho")
                expandable: true
                width: parent.width

                Column {
                    width: parent.width

                    Column {
                        visible: group_shortcuts.showContent
                        width: parent.width
                        spacing: 5

                        LabelOption {
                            Layout.fillWidth: true
                            text: qsTr("*No macOS a tecla Ctrl é substituída pela tecla Command")
                            font.bold: true
                            font.pixelSize: 10
                            font.capitalization: Font.AllUppercase
                            borderBottom: false
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "F1"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Exibe esta informação")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+N"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define o próximo Tempo da lista disponível.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+S"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Inicia a contagem do Tempo definido.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+P"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Pausa o cronômetro.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+R"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Remove o Tempo definido.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+G"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Encerra a contagem, salva o Tempo decorrido e zera as configurações.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+Shift+L"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Exibe o Popup Lista de Tempo")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+Shift+D"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Exibe o Popup Dispositivos Conectados/Conectar ao Controlador")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+,"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Exibe o Popup Configurações")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+Shift+C"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define o dispositivo como Controlador.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+Shift+R"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Define o dispositivo como Receptor.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Alt+C"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Tenta restabelecer a conexão com o Controlador.")
                            }
                        }

                        RowLayout {
                            width: parent.width
                            spacing: 10

                            Label {
                                text: "Ctrl+Q"
                                font.bold: true
                            }

                            LabelOption {
                                Layout.fillWidth: true
                                text: qsTr("Encerra o aplicativo")
                            }
                        }
                    }
                }
            }
        }
    }

    onClosed: deleteDialog()
}

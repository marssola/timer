import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.LocalStorage 2.0

import "../js/Database.js" as DB
import "./components"

Dialog {
    id: dialog
    property bool mobile

    title: qsTr("Lista de Tempo")
    width: window.width < 400? window.width -10 : 400
    height: window.height > 600? 600 : window.height - window.header.height - 10

    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    modal: true
    clip: true
    padding: 5

    standardButtons: Dialog.Close
    closePolicy: Popup.NoAutoClose

    contentItem: Flickable {
        anchors.fill: parent
        anchors.topMargin: header.height
        anchors.bottomMargin: footer.height

        ColumnLayout {
            id: column
            anchors.fill: parent
            anchors.margins: 5

            Button {
                Layout.fillWidth: true
                text: listView.draggable ? qsTr("Salvar alterações") : qsTr("Adicionar")

                onClicked: {
                    if (listView.draggable) {
                        for (var i = 0; i < list_defined_time.count; ++i) {
                            DB.updateDiscourse(list_defined_time.get(i).id, {'sorted' : i})
                        }
                        getListDefinedTime()
                        listView.draggable = false
                    } else {
                        dialogNewPlayList.open()
                    }
                }
            }

            ListView {
                id: listView
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible: list_defined_time.count
                clip: true

                property bool draggable: false

                model: list_defined_time
                delegate: SwipeDelegate {
                    id: swipeDelegate

                    width: parent.width
                    height: 60
                    highlighted: controllerPage.running_timer.id === index
                    z: 1
                    property var m_time: new Date(time * 1000)
                    property var m_elapsed : new Date(elapsed * 1000)
                    property var m_alert: new Date(alert * 1000)

                    padding: 0
                    spacing: 0

                    contentItem: RowLayout {
                        id: row_label

                        Label {
                            visible: listView.draggable
                            text: "\uE8FE"
                            font.family: material_icon.name
                            font.pixelSize: 25
                        }

                        ColumnLayout {
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            RowLayout {
                                Layout.fillWidth: true

                                Label {
                                    Layout.preferredWidth: 10
                                    Layout.fillHeight: true

                                    text: "\uE192"
                                    font.family: material_icon.name
                                    padding: 5
                                }
                                Label {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true

                                    text: completeZero(m_time.getUTCHours()) + ":" + completeZero(m_time.getUTCMinutes()) + ":" + completeZero(m_time.getUTCSeconds())
                                    padding: 5
                                }

                                Label {
                                    Layout.preferredWidth: 10
                                    Layout.fillHeight: true

                                    text: elapsed? "\uE422": ""
                                    font.family: material_icon.name
                                    padding: 5
                                }
                                Label {
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true

                                    text: elapsed? completeZero(m_elapsed.getUTCHours()) + ":" + completeZero(m_elapsed.getUTCMinutes()) + ":" + completeZero(m_elapsed.getUTCSeconds()) : ""
                                    padding: 5
                                }

                                Label {
                                    Layout.preferredWidth: 10
                                    Layout.fillHeight: true

                                    text: "\uE7F7"
                                    font.family: material_icon.name
                                    padding: 5
                                }
                                Label {
                                    Layout.fillHeight: true

                                    text: completeZero(m_alert.getUTCHours()) + ":" + completeZero(m_alert.getUTCMinutes()) + ":" + completeZero(m_alert.getUTCSeconds())
                                    padding: 5
                                }
                            }

                            RowLayout {
                                Layout.fillHeight: true

                                Label {
                                    Layout.fillWidth: true

                                    text: discourse
                                    padding: 5
                                    elide: Label.ElideMiddle
                                }

                                Label {
                                    visible: elapsed
                                    text: elapsed? "\uE86C" : ""
                                    font.family: material_icon.name
                                    color: Material.color(Material.Grey)
                                    padding: 5
                                }
                            }
                        }
                    }

                    Column {
                        width: parent.width
                        anchors.bottom: parent.bottom

                        Rectangle {
                            width: parent.width
                            height: 1
                            color: object.border_up
                        }
                        Rectangle {
                            width: parent.width
                            height: 1
                            color: object.border_down
                        }
                    }

                    swipe.left: Label {
                        id: shareLabel
                        visible: swipeDelegate.opacity == 1

                        text: "\uE80D"
                        font.family: material_icon.name
                        font.pixelSize: 20

                        anchors.left: parent.left
                        height: parent.height
                        width: height
                        verticalAlignment: Qt.AlignVCenter
                        padding: 15

                        background: Rectangle {
                            color: mousearea_swipedelegate_left.pressed ? "#333333" : "#111111"

                            MouseArea {
                                id: mousearea_swipedelegate_left
                                anchors.fill: parent
                                onClicked: {
                                    shareUtils.share(DB.getDiscourseListToShare(id));
                                    swipeDelegate.swipe.close()
                                }
                            }
                        }
                    }

                    swipe.right: Label {
                        id: deleteLabel
                        visible: swipeDelegate.opacity == 1

                        text: "\uE872"
                        font.family: material_icon.name
                        font.pixelSize: 20
                        color: Material.color(Material.Red)

                        anchors.right: parent.right
                        height: parent.height
                        width: height
                        verticalAlignment: Qt.AlignVCenter
                        padding: 15

                        background: Rectangle {
                            color: mousearea_swipedelegate.pressed ? "#333333" : "#111111"

                            MouseArea {
                                id: mousearea_swipedelegate
                                anchors.fill: parent
                                onClicked: {
                                    DB.removeDiscourse(id)
                                    listView.model.remove(index)
                                }
                            }
                        }
                    }

                    ListView.onRemove: SequentialAnimation {
                        PropertyAction {
                            target: swipeDelegate
                            property: "ListView.delayRemove"
                            value: true
                        }
                        NumberAnimation {
                            target: swipeDelegate
                            property: "height"
                            to: 0
                            easing.type: Easing.InOutQuad
                        }
                        PropertyAction {
                            target: swipeDelegate
                            property: "ListView.delayRemove"
                            value: false
                        }
                    }

                    onClicked: {
                        dialogEditPlayList.id = id
                        dialogEditPlayList.open()
                    }

                    MouseArea {
                        id: dragArea

                        property int positionStarted: 0
                        property int positionEnded: 0
                        property int positionsMoved: Math.floor(positionEnded - positionStarted) / swipeDelegate.height
                        property int newPosition: index + positionsMoved
                        property bool held: false

                        enabled: listView.draggable
                        anchors.fill: parent
                        drag.axis: Drag.YAxis

                        onPressed: {
                            positionEnded = swipeDelegate.y
                            swipeDelegate.z = 3
                            positionStarted = swipeDelegate.y
                            listView.interactive = false
                            drag.target = swipeDelegate
                            swipeDelegate.opacity = 0.5
                            held = true
                            drag.maximumY = (column.height - swipeDelegate.height - 1 + listView.contentY)
                            drag.minimumY = -10
                        }

                        onPositionChanged: positionEnded = swipeDelegate.y

                        onReleased: {
                            if (Math.abs(positionsMoved) < 1 && held) {
                                swipeDelegate.y = positionStarted
                            } else {
                                if (held) {
                                    if (newPosition < 1) {
                                        list_defined_time.move(index, 0, 1)
                                    } else if (newPosition > listView.count - 1) {
                                        list_defined_time.move(index, listView.count - 1, 1)
                                    } else {
                                        list_defined_time.move(index, newPosition, 1)
                                    }
                                }
                            }
                            swipeDelegate.z = 1
                            swipeDelegate.opacity = 1
                            listView.interactive = true
                            drag.target = null
                            held = false
                        }
                    }

                    onPressAndHold: listView.draggable = true
                }
            }

            RowLayout {
                visible: list_defined_time.count
                Layout.fillWidth: true

                Button {
                    Layout.fillWidth: true
                    text: "\uE872"
                    font.family: material_icon.name
                    font.pixelSize: 20
                    flat: true
                    Material.foreground: Material.Red

                    onPressed: popup_remove.open()

                    Popup {
                        id: popup_remove
                        width: 250
                        padding: 5

                        Column {
                            width: parent.width

                            Label {
                                width: parent.width
                                wrapMode: Label.WordWrap
                                text: qsTr("Deseja realmente remover todos os itens?")
                                color: Material.color(Material.Red, Material.Shade500)
                            }

                            RowLayout {
                                width: parent.width

                                Button {
                                    Layout.fillWidth: true
                                    text: qsTr("Sim")
                                    Material.background: Material.color(Material.Red, Material.Shade500)

                                    onClicked: {
                                        DB.removeAllDiscourses()
                                        getListDefinedTime()
                                        popup_remove.close()
                                    }
                                }

                                Button {
                                    Layout.fillWidth: true
                                    flat: true
                                    text: qsTr("Não")

                                    onClicked:  popup_remove.close()
                                }
                            }
                        }
                    }
                }

                Button {
                    Layout.fillWidth: true
                    text: "\uE8B3"
                    font.family: material_icon.name
                    font.pixelSize: 20
                    flat: true

                    onPressed: popup_zerar.open()

                    Popup {
                        id: popup_zerar
                        width: 250
                        x: -(width /2)
                        padding: 5

                        Column {
                            width: parent.width

                            Label {
                                width: parent.width
                                wrapMode: Label.WordWrap
                                text: qsTr("Deseja realmente zerar o tempo de todos os itens?")
                                color: Material.color(Material.Red, Material.Shade500)
                            }

                            RowLayout {
                                width: parent.width

                                Button {
                                    Layout.fillWidth: true
                                    text: qsTr("Sim")
                                    Material.background: Material.color(Material.Red, Material.Shade500)

                                    onClicked: {
                                        DB.zerarDiscourses()
                                        getListDefinedTime()
                                        popup_zerar.close()
                                    }
                                }

                                Button {
                                    Layout.fillWidth: true
                                    flat: true
                                    text: qsTr("Não")

                                    onClicked:  popup_zerar.close()
                                }
                            }
                        }
                    }
                }

                Button {
                    Layout.fillWidth: true
                    text: "\uE80D"
                    font.family: material_icon.name
                    font.pixelSize: 20
                    flat: true

                    onPressed: shareUtils.share(DB.getDiscoursesListToShare())
                }
            }

            Label {
                id: info_list
                Layout.fillWidth: true
                Layout.fillHeight: true
                visible: !list_defined_time.count

                text: qsTr("Nenhum tempo foi adicionada nesta lista...")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
                opacity: 0.7

                SequentialAnimation on opacity {
                    running: !list_defined_time.count

                    OpacityAnimator {
                        from: 0.7
                        to: 0
                        duration: 1000
                        loops: Animation.Infinite
                    }
                    OpacityAnimator {
                        from: 0
                        to: 0.7
                        duration: 1000
                        loops: Animation.Infinite
                    }
                }
            }
        }
    }

    DialogNewPlayList {
        id: dialogNewPlayList
    }

    DialogEditPlayList {
        id: dialogEditPlayList
    }

    onVisibleChanged: if (visible) getListDefinedTime()
    onClosed: deleteDialog()
}

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

Item {
    id: item
    property alias running_timer: running_timer
    property var m_time: running_timer.id > -1 && list_defined_time.get(running_timer.id)? new Date(list_defined_time.get(running_timer.id).time * 1000) : 0

    anchors.fill: parent

    Rectangle {
        id: rectangle_timer
        color: "transparent"
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent
            spacing: 0

            RowLayout {
                Layout.fillWidth: true
                Layout.preferredHeight: 48
                z: 2

                Label {
                    visible: running_timer.id > -1 && list_defined_time.get(running_timer.id)
                    text: "\uE192"
                    font.family: material_icon.name
                    Layout.topMargin: 5
                    Layout.leftMargin: 5
                }
                Label {
                    text: m_time && list_defined_time.get(running_timer.id)? completeZero(m_time.getUTCHours()) + ":" + completeZero(m_time.getUTCMinutes()) + ":" + completeZero(m_time.getUTCSeconds()) : ""
                    Layout.topMargin: 5
                }

                Label {
                    visible: running_timer.id > -1 && list_defined_time.get(running_timer.id)
                    text: "\uE91F"
                    font.family: material_icon.name
                    verticalAlignment: Qt.AlignVCenter
                    Layout.topMargin: 5
                    Layout.leftMargin: 5
                }
                Label {
                    text: running_timer.id > -1 && list_defined_time.get(running_timer.id)? list_defined_time.get(running_timer.id).discourse : ""
                    Layout.fillWidth: true
                    elide: Label.ElideMiddle
                    Layout.topMargin: 5
                    Layout.rightMargin: 5
                }
                RoundButton {
                    id: button_reset_timer
                    visible: running_timer.id > -1 && list_defined_time.get(running_timer.id)
                    enabled: object.timer_paused || !object.timer_started
                    text: "\uE14A"
                    font.family: material_icon.name
                    font.pixelSize: 15
                    flat: true
                    z: 20
                    onClicked: reset_timer()
                }
            }

            TimerComponent {
                id: running_timer
                z: 1

                property int id: -1
                Layout.fillHeight: true
                Layout.fillWidth: true
                blink: countdown.time_closed

                MouseArea {
                    anchors.fill: parent
                }
            }
        }

        SequentialAnimation on color {
            running: object.alert

            ColorAnimation {
                to: object.alert_color
                duration: object.alert
            }
        }

        SequentialAnimation on color {
            running: !object.alert

            ColorAnimation {
                to: "transparent"
                duration: 5000
            }
        }
    }
}

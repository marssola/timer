import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

import "./components"

Dialog {
    id: dialog

    property bool mobile
    property int margin: 20
    property int pixelSizeValue: 20

    title: qsTr("Configurações")
    width: window.width < 400? window.width -10 : 400
    height: window.height > 600? 600 : window.height - window.header.height - 10

    modal: true
    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    padding: 5
    clip: true

    standardButtons: Dialog.Close
    closePolicy: Popup.NoAutoClose

    contentItem: Flickable {
        anchors.fill: parent
        anchors.topMargin: header.height
        anchors.bottomMargin: footer.height
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            anchors.margins: 5

            Item {
                width: parent.width
                height: dialog.margin
            }

            Column {
                width: parent.width

                LabelSection {
                    text: qsTr("Dispositivo")
                }

                ItemDelegate {
                    width: parent.width

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.preferredWidth: 80
                            Layout.leftMargin: 20

                            text: qsTr("Nome")
                            font.bold: true
                        }

                        Label {
                            Layout.fillWidth: true
                            width: parent.width
                            Layout.rightMargin: 20
                            text: settings.local_name
                            font.pixelSize: dialog.pixelSizeValue
                            horizontalAlignment: Label.AlignRight
                            elide: Label.ElideRight
                        }
                    }

                    onClicked: loadDialog("DeviceName")
                }

                ItemDelegate {
                    width: parent.width

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.preferredWidth: 80
                            Layout.leftMargin: 20

                            text: qsTr("Endereço IP")
                            font.bold: true
                        }

                        Label {
                            Layout.fillWidth: true
                            width: parent.width
                            Layout.rightMargin: 20
                            text: host.getAddress().join(', ')
                            font.pixelSize: dialog.pixelSizeValue
                            horizontalAlignment: Label.AlignRight
                            elide: Label.ElideRight
                        }
                    }

                    onClicked: loadDialog("NetworkAddress")
                }

                Item {
                    width: parent.width
                    height: dialog.margin
                }
            }

            Column {
                width: parent.width

                LabelSection {
                    text: qsTr("App")
                }

                SwitchDelegate {
                    id: device_type

                    width: parent.width
                    text: qsTr("Controlador")
                    checked: settings.type

                    onCheckedChanged: settings.type = checked
                }

                ItemDelegate {
                    visible: settings.type
                    enabled: settings.type
                    width: parent.width

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.preferredWidth: 80
                            Layout.leftMargin: 20

                            text: qsTr("PIN")
                            font.bold: true
                        }

                        Label {
                            Layout.fillWidth: true
                            width: parent.width
                            Layout.rightMargin: 20
                            text: settings.pin
                            font.pixelSize: dialog.pixelSizeValue
                            horizontalAlignment: Label.AlignRight
                            elide: Label.ElideRight
                        }
                    }

                    onClicked: loadDialog("PinCode")
                }

                Item {
                    width: parent.width
                    height: dialog.margin
                }
            }

            Column {
                width: parent.width

                LabelSection {
                    text: qsTr("Layout")
                }

                SwitchDelegate {
                    width: parent.width
                    text: qsTr("Otimizar para telas OLED")

                    checked: settings.oled_display
                    onCheckedChanged: settings.oled_display = checked
                }

                Item {
                    width: parent.width
                    height: dialog.margin
                }
            }

            Column {
                width: parent.width
                visible: !object.mobile

                LabelSection {
                    text: qsTr("Tela / Janela")
                }

                SwitchDelegate {
                    id: switch_window
                    width: parent.width
                    text: qsTr("Remover barra de titulo<br/>e botões da janela")

                    checked: settings.flag_window
                    onCheckedChanged: {
                        if (checked)
                            switch_fullscreen.checked = false
                        settings.flag_window = checked
                    }
                }

                SwitchDelegate {
                    id: switch_fullscreen
                    width: parent.width
                    text: qsTr("Tela cheia")

                    checked: settings.flag_window_fullscreen
                    onCheckedChanged: {
                        if (checked)
                            switch_window.checked = false
                        settings.flag_window_fullscreen = checked
                    }
                }

                Item {
                    width: parent.width
                    height: dialog.margin
                }
            }

            Column {
                width: parent.width

                LabelSection {
                    text: ""
                }

                ItemDelegate {
                    width: parent.width
                    text: qsTr("Reconfigurar o App")

                    onClicked: popup_reconfigure.open()

                    Popup {
                        id: popup_reconfigure
                        width: parent.width * 0.8
                        padding: 5
                        y: -50
                        x: (parent.width - width) / 2

                        Column {
                            width: parent.width

                            Label {
                                text: qsTr("Tem certeza que deseja reconfigurar o App?")
                                color: Material.color(Material.Red, Material.Shade500)
                            }

                            RowLayout {
                                width: parent.width

                                Button {
                                    Layout.fillWidth: true
                                    text: qsTr("Sim")
                                    Material.background: Material.color(Material.Red, Material.Shade500)

                                    onClicked: {
                                        window.reconfigure()
                                        popup_reconfigure.close()
                                        dialog.close()
                                    }
                                }

                                Button {
                                    Layout.fillWidth: true
                                    flat: true
                                    text: qsTr("Não")

                                    onClicked:  popup_reconfigure.close()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: loader
        asynchronous: true
        active: false
    }

    function loadDialog(page, parameters)
    {
        if (parameters === undefined)
            parameters = { visible: true, mobile: object.mobile }
        else
        {
            parameters.visible = true
            parameters.mobile = object.mobile
        }

        loader.setSource("Dialog" + page + ".qml", parameters)
        loader.active = true
    }

    onClosed: deleteDialog()
}

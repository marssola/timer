import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.LocalStorage 2.0

import "../js/Database.js" as DB
import "./components"

Dialog {
    id: dialog
    title: qsTr("Editar Tempo")

    parent: ApplicationWindow.overlay
    width: window.width < 400? window.width -10 : 400
    height: window.height > 370? 370 : window.height - window.header.height - 10

    modal: true
    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    padding: 5
    clip: true

    closePolicy: Popup.NoAutoClose
    property int id: -1
    property var discourse

    onIdChanged: {
        discourse = DB.getDiscourse(id)
        if (!discourse.id)
            return

        var m_time = new Date(discourse.time * 1000)
        var m_elapsed = new Date(discourse.elapsed * 1000)
        var m_alert = new Date(discourse.alert * 1000)

        add_discourse_name.text = discourse.discourse
        add_timer.setHours.currentIndex = m_time.getUTCHours()
        add_timer.setMinutes.currentIndex = m_time.getUTCMinutes()
        add_timer.setSeconds.currentIndex = m_time.getUTCSeconds()

        add_alert.setHours.currentIndex = m_alert.getUTCHours()
        add_alert.setMinutes.currentIndex = m_alert.getUTCMinutes()
        add_alert.setSeconds.currentIndex = m_alert.getUTCSeconds()

        elapsed_time.setHours.currentIndex = m_elapsed.getUTCHours()
        elapsed_time.setMinutes.currentIndex = m_elapsed.getUTCMinutes()
        elapsed_time.setSeconds.currentIndex = m_elapsed.getUTCSeconds()
    }

    contentItem: Flickable {
        width: parent.width
        height: parent.height
        contentHeight: column.implicitHeight

        Column {
            id: column
            width: parent.width
            anchors.margins: 5

            RowLayout {
                width: parent.width - 10
                height: 60

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true
                    text: "\uE91F"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TextField {
                    id: add_discourse_name
                    Layout.fillWidth: true
                }
            }

            RowLayout {
                width: parent.width
                height: 70

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true
                    text: "\uE924"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TimerComponent {
                    id: add_timer
                    itemWidth: 200
                    Layout.fillHeight: true
                    textSize: 16
                }

                Item {
                    Layout.preferredWidth: 64
                }
            }

            RowLayout {
                width: parent.width
                height: 70

                Label {
                    Layout.preferredWidth: 60
                    Layout.fillHeight: true
                    text: "\uE7F7"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TimerComponent {
                    id: add_alert
                    itemWidth: 200
                    Layout.fillHeight: true
                    textSize: 16
                }

                Item {
                    Layout.preferredWidth: 64
                }
            }

            RowLayout {
                width: parent.width
                height: 70

                Label {
                    Layout.preferredWidth: 50
                    Layout.fillHeight: true
                    text: "\uE422"
                    font.family: material_icon.name
                    font.pixelSize: 30

                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                TimerComponent {
                    id: elapsed_time
                    itemWidth: 200
                    Layout.fillHeight: true
                    textSize: 16
                    opacity: 0.7

                    MouseArea {
                        anchors.fill: parent
                    }
                }

                RoundButton {
                    flat: true
                    text: "\uE8B3"
                    font.family: material_icon.name
                    font.pixelSize: 25

                    onClicked: {
                        elapsed_time.setHours.currentIndex = 0
                        elapsed_time.setMinutes.currentIndex = 0
                        elapsed_time.setSeconds.currentIndex = 0
                    }
                }
            }
        }
    }

    footer: DialogButtonBox {
        Button {
            text: qsTr("Cancel")
            flat: true

            onClicked: {
                dialog.close()
                dialog.id = -1
            }
        }

        Button {
            text: qsTr("Ok")
            flat: true

            onClicked: {
                var error = false
                var m_message = []
                if (add_discourse_name.text === "")
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o Discurso/Orador"))
                    add_discourse_name.focus = true
                }

                if (add_timer.getHours == 0 && add_timer.getMinutes == 0 && add_timer.getSeconds == 0)
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o tempo"))
                }

                if (add_alert.getHours == 0 && add_alert.getMinutes == 0 && add_alert.getSeconds == 0)
                {
                    error = true;
                    m_message.push(qsTr("É obrigatório informar o tempo de alerta"))
                }

                if (error)
                {
                    object.notification = m_message.join("<br/>")
                    return;
                }

                DB.updateDiscourse(id, {
                   "discourse": add_discourse_name.text,
                   'time': new Date("1970-01-01 "+completeZero(add_timer.getHours)+":"+completeZero(add_timer.getMinutes)+":"+completeZero(add_timer.getSeconds)+" -0000").getTime() /1000,
                   'alert': new Date("1970-01-01 "+completeZero(add_alert.getHours)+":"+completeZero(add_alert.getMinutes)+":"+completeZero(add_alert.getSeconds)+" -0000").getTime() /1000,
                   'elapsed': (new Date("1970-01-01 "+completeZero(add_timer.getHours)+":"+completeZero(add_timer.getMinutes)+":"+completeZero(add_timer.getSeconds)+" -0000").getTime() /1000 !== discourse.time ||
                               new Date("1970-01-01 "+completeZero(add_alert.getHours)+":"+completeZero(add_alert.getMinutes)+":"+completeZero(add_alert.getSeconds)+" -0000").getTime() /1000 !== discourse.alert)? 0 : new Date("1970-01-01 "+completeZero(elapsed_time.getHours)+":"+completeZero(elapsed_time.getMinutes)+":"+completeZero(elapsed_time.getSeconds)+" -0000").getTime() /1000
                })
                getListDefinedTime()
                dialog.close()
                dialog.id = -1
            }
        }
    }
}

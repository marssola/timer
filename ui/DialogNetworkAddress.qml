import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2

import "./components"

Dialog {
    id: dialog
    property bool mobile

    title: qsTr("Endereços de rede")

    parent: ApplicationWindow.overlay
    width: window.width < 300? window.width -10 : 300
    height: 200

    modal: true
    x: (window.width - width) /2
    y: (window.height - height) /2 - window.header.height
    padding: 1
    clip: true

    standardButtons: Dialog.Close

    contentItem: Column {
        anchors.fill: parent
        anchors.topMargin: header.height
        anchors.bottomMargin: footer.height

        ListView {
            width: parent.width
            height: parent.height

            model: list_address
            delegate: ItemDelegate {
                width: parent.width
                text: address
            }
        }

        ListModel {
            id: list_address
        }
    }

    onVisibleChanged: {
        if (visible)
        {
            list_address.clear()
            for (var i = 0; i < host.getAddress().length; ++i)
                list_address.append({address : host.getAddress()[i]})
        }
    }

    /*
    contentItem: Item {
        width: parent.width
        height: parent.height

        Label {
            anchors.fill: parent
            font.pixelSize: 18
            anchors.margins: 10
            text: host.getAddress().join('<br/>')
        }
    }
    */
}

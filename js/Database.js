var db = LocalStorage.openDatabaseSync("TimerDB", "1.0", "Database to store discourses list and info of timer", 1000000);
var error = "";

function init()
{
    try {
        db.transaction(function (tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS DiscoursesList(id INTEGER PRIMARY KEY AUTOINCREMENT, discourse TEXT, time INT, alert INT, elapsed INT, sorted INT)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS Timer(id INTEGER PRIMARY KEY AUTOINCREMENT, discourse_id INT, running INT, defined INT, start INT, end INT)");
        });

    } catch (err) {
        console.log("Error trying to create tables: " + err)
    }
}

function destroy()
{
    try {
        db.transaction(function (tx) {
            tx.executeSql("DROP TABLE IF EXISTS DiscoursesList")
            tx.executeSql("DROP TABLE IF EXISTS Timer")
        });
    } catch (err) {
        console.log("Error trying to drop table: " + err)
    }
}

function setDiscourseList(data)
{
    try {
        db.transaction(function (tx) {
            tx.executeSql("INSERT INTO DiscoursesList (discourse, time, alert, elapsed, sorted) VALUES (\'" + data.join("\', \'") + "\')");
        });
    } catch (err) {
        console.log("Error trying to insert discourse: " + err);
    }
}

function getDiscoursesList()
{
    var discourses = [];
    db.transaction(function (tx) {
        var list = tx.executeSql("SELECT * FROM DiscoursesList ORDER BY sorted ASC");
        if (list.rows.length) {
            discourses = list.rows;
        }
    });
    return discourses;
}

function getDiscourse(id)
{
    var discourse = {};
    db.transaction(function (tx) {
        var get = tx.executeSql("SELECT * FROM DiscoursesList WHERE id=?", [id]);
        if (get.rows.length)
            discourse = get.rows.item(0);
    });
    return discourse;
}

function getDiscoursesListToShare()
{
    var text_share = "";
    for (var i = 0; i < list_defined_time.count; ++i)
    {
        var discourse = list_defined_time.get(i);
        var m_time = new Date(discourse.time * 1000)
        var m_elapsed = new Date(discourse.elapsed * 1000)

        text_share += "Discurso/Orador : " + discourse.discourse + "\n";
        text_share += "Tempo definido  : " + completeZero(m_time.getUTCHours()) + ":" + completeZero(m_time.getUTCMinutes()) + ":" + completeZero(m_time.getUTCSeconds()) + "\n";
        text_share += "Tempo decorrido : " + completeZero(m_elapsed.getUTCHours()) + ":" + completeZero(m_elapsed.getUTCMinutes()) + ":" + completeZero(m_elapsed.getUTCSeconds()) + "\n";
        text_share += "\n";
    }
    return text_share;
}

function getDiscourseListToShare(id)
{
    var discourse = getDiscourse(id);
    var text_share = "";

    var m_time = new Date(discourse.time * 1000)
    var m_elapsed = new Date(discourse.elapsed * 1000)

    text_share += "Discurso/Orador : " + discourse.discourse + "\n";
    text_share += "Tempo definido  : " + completeZero(m_time.getUTCHours()) + ":" + completeZero(m_time.getUTCMinutes()) + ":" + completeZero(m_time.getUTCSeconds()) + "\n";
    text_share += "Tempo decorrido : " + completeZero(m_elapsed.getUTCHours()) + ":" + completeZero(m_elapsed.getUTCMinutes()) + ":" + completeZero(m_elapsed.getUTCSeconds()) + "\n";

    return text_share;
}

function updateDiscourse(id, data)
{
    var result = false, data_update = [], query;
    for (var key in data)
        data_update.push(key + "=" + (typeof data[key] === 'number' ? data[key] : "'" + data[key] + "'") + "");
    query = "UPDATE DiscoursesList SET " + data_update.join(', ') + " WHERE id=" + id;

    db.transaction(function (tx) {
        try {
            tx.executeSql(query);
            result = true;
        } catch (err) {
            console.log("Error trying to update discourse: " + err);
        }
    });
    return result;
}

function zerarDiscourses()
{
    var status = false;
    db.transaction(function (tx) {
        try {
            tx.executeSql("UPDATE DiscoursesList SET elapsed=0 WHERE id>0");
            status = true;
        } catch (err) {
            console.log("Error trying to delete discourse: " + err);
        }
    });

    return status;
}

function removeDiscourse(id)
{
    var status = false;
    db.transaction(function (tx) {
        try {
            tx.executeSql("DELETE FROM DiscoursesList WHERE id=?", [id]);
            status = true;
        } catch (err) {
            console.log("Error trying to delete discourse: " + err);
        }
    });

    return status;
}

function removeAllDiscourses()
{
    var status = false;
    db.transaction(function (tx) {
        try {
            tx.executeSql("DELETE FROM DiscoursesList WHERE id>0");
            status = true;
        } catch (err) {
            console.log("Error trying to delete all discourses: " + err);
        }
    });

    return status;
}

function getTimerRunning()
{
    var result = [];
    db.transaction(function (tx) {
        var get = tx.executeSql("SELECT * FROM Timer ORDER BY id DESC LIMIT 1");
        if (get.rows.length)
            result = get.rows.item(0);
    });
    return result;
}

function prepareTimer(discourse_id)
{
    db.transaction(function (tx) {
        try {
            removeTimerRunning()
            tx.executeSql("INSERT INTO Timer (discourse_id, running) VALUES(?, 0)", [discourse_id]);
        } catch (err) {
            console.log("Error trying to prepare timer: " + err);
        }
    });
}

function setTimerRunning(data)
{
    var obj = {
        key: [],
        value: []
    };
    for (var k in data) {
        obj.key.push(k);
        obj.value.push((typeof data[k] === "number"? data[k] : "'" + data[k] + "'"));
    }

    db.transaction(function (tx) {
        try {
            removeTimerRunning()
            tx.executeSql("INSERT INTO Timer (" + obj.key.join(', ') + ") VALUES(" + obj.value.join(', ') + ")");
        } catch (err) {
            console.log("Error trying to set timer running: " + err);
        }
    });
}

function removeTimerRunning()
{
    db.transaction(function (tx) {
        try {
            tx.executeSql("DELETE FROM Timer WHERE id > 0");
        } catch (err) {
            console.log("Error trying to delete timer running");
        }
    });
}

function completeZero(str, length)
{
    if (length === undefined)
        length = 2;
    str = str.toString();
    while (str.length < length) {
        str = "0" + str;
    }
    return str;
}

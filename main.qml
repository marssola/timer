import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0

import StatusBar 0.1
import HostInfo 0.1
import NetworkDiscovery 0.1
import TcpConnect 0.1
import Countdown 0.1
import Share 0.1

import "./js/Database.js" as DB
import "./ui"

ApplicationWindow {
    id: window
    visible: true
    width: 360
    height: 660
    title: Qt.application.name
    font: ubuntu.name

    QtObject {
        id: object

        property bool busy: false
        property bool timer_started: false
        property bool timer_paused: false
        property int _time: 0
        property int _counttimer: 0
        property int alert: countdown.alert
        property double alert_alpha: 0
        property color alert_color: Material.color(Material.Red, Material.Shade900)
        property color border_up: settings.oled_display? "#090909" : "#101010"
        property color border_down: settings.oled_display? "#191919" : "#2f2f2f"
        property color background: settings.oled_display? "#000000" : "#222222"
        property string notification: ""
        property bool mobile: Qt.platform.os === "android" || Qt.platform.os === "ios"

        onNotificationChanged: {
            if (notification !== "") {
                message._text = notification
                message.visible = true
            }
        }
    }

    Material.accent: Material.color(Material.Red, Material.Shade500)
    Material.background: object.background

    Settings {
        id: settings

        property bool flag_window: false
        onFlag_windowChanged: platformFlags()
        property bool flag_window_fullscreen: false
        onFlag_window_fullscreenChanged: flag_window_fullscreen? window.showFullScreen() : window.showMaximized()
        property bool oled_display: false

        property int type: 0
        property int pin: 0
        property bool first_use: true
        property bool pin_manual: false
        property bool controller_manual: false
        property string controller_name: ""
        property string controller_addr: ""
        property int controller_pin: 0
        property int controller_alert: 0

        property string local_name: ""
        property string local_addr: ""

        property bool timer_configured_running: false
        property int timer_configured_time: 0
        property int timer_configured_time_start: 0

        Component.onCompleted: {
            if (pin == 0 || pin === "")
                pin = host.getPin();

            if (controller_addr !== "" && controller_pin > 0)
                tcp_connect.client_connectController();
        }

        onTypeChanged: {
            if (type) {
                DB.init()
            }
        }

        onController_alertChanged: countdown.timeToString(controller_alert);
    }

    Share {
        id: shareUtils

        onShareContent: if (!object.mobile) object.notification = qsTr("Enviado para a área de transferência")
    }

    ListModel {
        id: list_defined_time
    }

    Shortcut {
        sequence: "Ctrl+Shift+C"
        onActivated: settings.type = 1
    }

    Shortcut {
        sequence: "Ctrl+Shift+R"
        onActivated: settings.type = 0
    }

    Shortcut {
        sequence: "Alt+C"
        onActivated: if (!settings.type) dialogConnection.validateFields()
    }

    Shortcut {
        sequence: "Ctrl+S"
        enabled: settings.type && list_defined_time.count && controllerPage !== undefined && controllerPage.running_timer.id > -1
        onActivated: if (!object.timer_started) button_start_pause.clicked()
    }

    Shortcut {
        sequence: "Ctrl+P"
        enabled: settings.type && object.timer_started
        onActivated: button_start_pause.pressAndHold()
    }

    Shortcut {
        sequence: "Ctrl+R"
        enabled: settings.type && object.timer_paused || !object.timer_started
        onActivated: reset_timer()
    }

    Shortcut {
        sequence: "Ctrl+G"
        enabled: settings.type && object.timer_started
        onActivated: button_stop_timer.pressAndHold()
    }

    Shortcut {
        sequence: "Ctrl+N"
        enabled: settings.type && object.timer_paused || !object.timer_started
        onActivated: button_set_reset_time.clicked()
    }

    Shortcut {
        sequence: "Ctrl+Shift+L"
        enabled: settings.type
        onActivated: openDialog("PlayList")
    }

    Shortcut {
        sequence: "Ctrl+Shift+D"
        enabled: settings.type
        onActivated: openDialog("Connection")
    }

    Shortcut {
        sequence: "Ctrl+,"
        onActivated: openDialog("Settings")
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }

    Shortcut {
        sequence: "F1"
        onActivated: openDialog("Help")
    }

    header: ToolBar {
        Material.primary: object.alert_color
        topPadding: Qt.platform.os === "ios" ? Screen.height - Screen.desktopAvailableHeight : 0

        RowLayout {
            anchors.fill: parent

            Label {
                text: "\uE924"
                font.family: material_icon.name
                font.pixelSize: 32
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                Layout.topMargin: 3
            }

            Label {
                text: "Timer"
                font.pixelSize: 24
                font.weight: Font.Light
                Layout.fillWidth: true
                Layout.topMargin: 0
                elide: Label.ElideRight
            }

            RowLayout {
                Layout.fillHeight: true

                ToolButton {
                    enabled: settings.type == 1
                    visible: enabled
                    Layout.fillHeight: true
                    text: "\uE05F"
                    font.family: material_icon.name
                    font.pixelSize: 28

                    onClicked: openDialog("PlayList")
                }

                ToolButton {
                    Layout.fillHeight: true
                    text: "\uE337"
                    font.family: material_icon.name
                    font.pixelSize: 28

                    Rectangle {
                        width: 16
                        height: width
                        radius: width
                        anchors.right: parent.right
                        anchors.rightMargin: 5
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 5
                        color: Qt.rgba(255, 255, 255, 0.80)

                        Text {
                            visible: settings.type
                            anchors.fill: parent
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            text: tcp_connect.devices
                            font.pixelSize: 12
                            font.bold: true
                            color: object.alert_color
                        }

                        Rectangle {
                            visible: !settings.type
                            width: 8
                            height: width
                            radius: width
                            anchors.centerIn: parent
                            color: tcp_connect.receiver_connect? Material.color(Material.Green, Material.Shade600) : object.alert_color
                        }
                    }

                    onClicked: openDialog("Connection")
                    onPressAndHold: {
                        if (settings.type)
                        {
                            object.notification = qsTr("Conferindo status dos receptores, aguarde...")
                            tcp_connect.server_writeSocket({});
                        }
                        else
                        {
                            validateFields();
                            countdown.prepareStopTime();
                            controllerPage.running_timer.id = -1
                        }

                    }
                }

                ToolButton {
                    Layout.fillHeight: true
                    text: "\uE5D4"
                    font.family: material_icon.name
                    font.pixelSize: 28

                    onPressed: menuApp.open()

                    Menu {
                        id: menuApp
                        y: parent.height * 0.8

                        MenuItem {
                            RowLayout {
                                anchors.fill: parent

                                Label {
                                    Layout.fillHeight: true
                                    Layout.leftMargin: 10
                                    Layout.rightMargin: 5

                                    text: "\uE8B8"
                                    font.pixelSize: 16
                                    font.family: material_icon.name
                                    verticalAlignment: Label.AlignVCenter
                                }

                                Label {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    Layout.rightMargin: 10
                                    width: parent.width
                                    elide: Label.ElideRight

                                    text: qsTr("Configurações")
                                    font.pixelSize: 16
                                    verticalAlignment: Label.AlignVCenter
                                }
                            }

                            onTriggered: openDialog("Settings")
                        }
                        MenuItem {
                            RowLayout {
                                anchors.fill: parent

                                Label {
                                    Layout.fillHeight: true
                                    Layout.leftMargin: 10
                                    Layout.rightMargin: 5

                                    text: "\uE887"
                                    font.pixelSize: 16
                                    font.family: material_icon.name
                                    verticalAlignment: Label.AlignVCenter
                                }

                                Label {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    Layout.rightMargin: 10
                                    width: parent.width
                                    elide: Label.ElideRight

                                    text: qsTr("Ajuda")
                                    font.pixelSize: 16
                                    verticalAlignment: Label.AlignVCenter
                                }
                            }

                            onClicked: openDialog("Help")
                        }
                        MenuItem {
                            RowLayout {
                                anchors.fill: parent

                                Label {
                                    Layout.fillHeight: true
                                    Layout.leftMargin: 10
                                    Layout.rightMargin: 5

                                    text: "\uE88E"
                                    font.pixelSize: 16
                                    font.family: material_icon.name
                                    verticalAlignment: Label.AlignVCenter
                                }

                                Label {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    Layout.rightMargin: 10
                                    width: parent.width
                                    elide: Label.ElideRight

                                    text: qsTr("Sobre")
                                    font.pixelSize: 16
                                    verticalAlignment: Label.AlignVCenter
                                }
                            }
                            onClicked: openDialog("About")
                        }
                    }
                }
            }
        }
    }

    footer: ToolBar {
        enabled: settings.type === 1
        visible: settings.type === 1
        height: 100
        Material.background: object.background

        RowLayout {
            anchors.fill: parent

            Item {
                Layout.fillHeight: true
                Layout.preferredWidth: 50

                RoundButton {
                    id: button_stop_timer
                    visible: object.timer_started
                    Layout.fillHeight: true
                    anchors.centerIn: parent
                    text: "\uE161"
                    font.family: material_icon.name
                    font.pixelSize: 25

                    onClicked: {
                        if (object.timer_paused)
                            stop_timer()
                        else
                            object.notification = qsTr("Pressione e segure para gravar o tempo")
                    }

                    onPressAndHold: stop_timer()
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 100

                RoundButton {
                    id: button_start_pause
                    enabled: list_defined_time.count && controllerPage && controllerPage.running_timer.id > -1
                    width: 90
                    height: width

                    anchors.centerIn: parent
                    Material.background: Material.color((object.timer_started? Material.Amber : Material.Green), Material.Shade500)
                    Material.elevation: 10
                    text: object.timer_started? "\uE034" : "\uE037"
                    font.family: material_icon.name
                    font.pixelSize: Math.round(parent.height * 0.65)

                    onClicked: start_timer();
                    onPressAndHold: pause_timer();
                }
            }

            Item {
                Layout.preferredHeight: 100
                Layout.preferredWidth: 50
                Layout.rightMargin: 5

                RoundButton {
                    id: button_set_reset_time
                    visible: list_defined_time.count
                    enabled: object.timer_paused || !object.timer_started
                    Layout.fillHeight: true
                    anchors.centerIn: parent

                    text: object.timer_paused? "\uE8B3" : "\uE044"
                    font.family: material_icon.name
                    font.pixelSize: 40

                    onClicked: {
                        if (object.timer_paused)
                            countdown.prepareStopTime()
                        setTime();
                    }
                }
            }
        }
    }

    ReceiverPage {
        id: receiverPage
        visible: !settings.type
        enabled: !settings.type
    }

    ControllerPage {
        id: controllerPage
        visible: settings.type
        enabled: settings.type
    }

    ToolTip {
        id: message
        property string _text

        timeout: 5000
        topMargin: parent.height -tooltip_text.parent.height -50
        x: (window.width - tooltip_text.parent.width) /2 -20
        z: 100

        width: 0
        height: 0

        Rectangle {
            width: tooltip_text.contentWidth + 10
            height: tooltip_text.contentHeight + 10

            color: Qt.rgba(Material.background.r, Material.background.g, Material.background.b, 0.8)
            radius: 3

            Column {
                anchors.fill: parent
                padding: 5

                Text {
                    id: tooltip_text
                    width: window.width *0.8
                    height: contentHeight
                    anchors.margins: 5

                    text: message._text
                    color: Material.foreground
                    wrapMode: Text.WordWrap
                }
            }
        }
    }

    Connections {
        target: message
        onVisibleChanged: if (!message.visible) object.notification = ""
    }

    Rectangle {
        visible: object.busy
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.8)
        z: 3

        BusyIndicator {
            id: busy_indicator
            anchors.centerIn: parent
        }
        Text {
            width: parent.width
            anchors.top: busy_indicator.bottom
            horizontalAlignment: Text.AlignHCenter

            color: "#fff"
            text: qsTr("aguarde...")
        }
    }

    HostInfo {
        id: host

        onPinChanged: settings.pin = getPin();
    }

    Countdown {
        id: countdown
        time_alert: settings.controller_alert
        isController: settings.type

        onSecondsChanged: (settings.type == 0)? receiverPage.running_timer.setSeconds.currentIndex = seconds :
                                                controllerPage.running_timer.setSeconds.currentIndex = seconds
        onMinutesChanged: (settings.type == 0)? receiverPage.running_timer.setMinutes.currentIndex = minutes :
                                                controllerPage.running_timer.setMinutes.currentIndex = minutes
        onHoursChanged: (settings.type == 0)? receiverPage.running_timer.setHours.currentIndex = hours :
                                              controllerPage.running_timer.setHours.currentIndex = hours
        onStatus_timerChanged: object.timer_started = status_timer
        onTimerPauseChanged: object.timer_paused = timerPause
        onSend_timerChanged: tcp_connect.setData_send(send_timer)
        onSend_commandChanged: tcp_connect.setData_send(send_command)
        onSend_timerIfRunningChanged: tcp_connect.setData_sendIfRunning(send_timerIfRunning)
        onBusyChanged: object.busy = busy

        onSaveTimerRunningChanged: DB.setTimerRunning(saveTimerRunning)
    }

    Timer {
        id: timer
        interval: 1000
        repeat: true
        onTriggered: {
            ++object._counttimer;
            var str_time = new Date(object._counttimer * 1000);
            counttimer.setHours.currentIndex = str_time.getUTCHours();
            counttimer.setMinutes.currentIndex = str_time.getUTCMinutes();
            counttimer.setSeconds.currentIndex = str_time.getUTCSeconds();
        }
    }

    NetworkDiscovery {
        id: networkDiscovery
        type: settings.type
        device: settings.local_name

        onConnect_controllerChanged: tcp_connect.connectToController(connect_controller);
    }

    TcpConnect {
        id: tcp_connect
        serviceType: settings.type
        local_addr: settings.local_addr
        addressController: settings.controller_addr
        pinPass: settings.pin
        pinController: settings.controller_pin
        deviceName: settings.local_name

        onReceive_current_time_controllerChanged: countdown.getCurrentTimeController(receive_current_time_controller)
        onReceive_timerChanged: countdown.setTimeFromController(receive_timer)
        onReceive_commandChanged: countdown.getCommand(receive_command)
        onAddressControllerChanged: settings.controller_addr = addressController
        onNameControllerChanged: settings.controller_name = nameController
        onSend_TimerIfRunning: countdown.prepareRequestTimerIfRunning()

        onBusyChanged: object.busy = busy
        onNotificationChanged: object.notification = notification
    }

    Connections {
        target: settings
    }

    StatusBar {
        id: statusBar
        theme: Material.Dark
        color: object.alert_color
    }

    FontLoader {
        id: ubuntu
        source: "qrc:/fonts/Ubuntu.ttf"
    }

    FontLoader {
        id: material_icon
        source: "qrc:/fonts/MaterialIcons.ttf"
    }

    Timer {
        id: loop_timer
        interval: 30 * 1000
        repeat: true
        running: object.timer_started

        onTriggered: tcp_connect.server_writeSocket({});
    }

    Loader {
        id: loader_ControllerPage
        asynchronous: true
        active: false
    }

    Loader {
        id: loader_dialog
        asynchronous: true
        active: false
    }

    function openDialog(page, parameters)
    {
        if (parameters === undefined)
            parameters = { visible: true, mobile: object.mobile }
        else
        {
            parameters.visible = true
            parameters.mobile = object.mobile
        }

        loader_dialog.setSource("ui/Dialog" + page + ".qml", parameters)
        loader_dialog.active = true
    }

    function deleteDialog()
    {
        loader_dialog.setSource("")
        loader_dialog.active = false
        gc()
    }

    function start_timer()
    {
        if (controllerPage.running_timer.id < 0)
            return;
        settings.controller_alert = list_defined_time.get(controllerPage.running_timer.id).alert
        if (!object.timer_started) {
            countdown.setTimeString({
                                        time: completeZero(controllerPage.running_timer.getHours) + ":" +
                                              completeZero(controllerPage.running_timer.getMinutes) + ":" +
                                              completeZero(controllerPage.running_timer.getSeconds),
                                        discourse_id: list_defined_time.get(controllerPage.running_timer.id).id
                                    })
        }
    }

    function stop_timer()
    {
        if (controllerPage.running_timer.id > -1)
        {
            DB.updateDiscourse(list_defined_time.get(controllerPage.running_timer.id).id, {
               'elapsed' :
                    (
                        getTimeSeconds("1970-01-01 " +
                        completeZero(controllerPage.running_timer.getHours) + ":" +
                        completeZero(controllerPage.running_timer.getMinutes) + ":" +
                        completeZero(controllerPage.running_timer.getSeconds))
                    )
           });
            getListDefinedTime()
        }
        reset_timer()
    }

    function reset_timer()
    {
        countdown.prepareStopTime()
        DB.removeTimerRunning()
        controllerPage.running_timer.id = -1
    }

    function setTime()
    {
        for (var k = 0; k < list_defined_time.count; ++k)
        {
            if (list_defined_time.get(k).elapsed > 0)
                continue
            var date = new Date(list_defined_time.get(k).time * 1000)
            controllerPage.running_timer.setHours.currentIndex = date.getUTCHours()
            controllerPage.running_timer.setMinutes.currentIndex = date.getUTCMinutes()
            controllerPage.running_timer.setSeconds.currentIndex = date.getUTCSeconds()
            controllerPage.running_timer.id = k
            DB.prepareTimer(list_defined_time.get(k).id)
            break
        }
    }

    function pause_timer()
    {
        if (object.timer_started && !object.timer_paused)
            countdown.preparePauseTime()
    }

    function action(_action)
    {
        if (_action === "start") {
            object.timer_started = true
            object._time = getTimeSeconds(
                        "1970-01-01 "
                        + completeZero(timer_prepare.getHours, 2) + ":"
                        + completeZero(timer_prepare.getMinutes, 2) + ":"
                        + completeZero(timer_prepare.getSeconds, 2)
                        )
            object._counttimer = 0
            timer.running = true
        } else if (_action === "stop") {
            object.timer_started = false
            object._counttimer = 0
            object._time = 0
        }
    }

    function getTimeSeconds(str)
    {
        var time = new Date(str + " -0000")
        return time.getTime() /1000
    }

    function completeZero(str, length)
    {
        if (length === undefined)
            length = 2
        str = str.toString()
        while (str.length < length) {
            str = "0" + str
        }
        return str;
    }

    function platformFlags()
    {
        if (Qt.platform.os === "osx" || Qt.platform.os === "windows" || Qt.platform.os === "linux")
        {
            window.flags = settings.flag_window? Qt.Window | Qt.FramelessWindowHint : 1
            window.showNormal()
            window.showMaximized()
        }
        if (Qt.platform.os === "ios")
            window.flags = Qt.Window | Qt.MaximizeUsingFullscreenGeometryHint
    }

    function getListDefinedTime()
    {
        var discourse_list = DB.getDiscoursesList()
        list_defined_time.clear()
        for (var x = 0; x < discourse_list.length; ++x) {
            list_defined_time.append(discourse_list.item(x))
        }
    }

    function validateFields()
    {
        if (tcp_connect.receiver_connect) {
            tcp_connect.client_disconnectController()
        } else {
            var validate = 0
            var _message = ""

            if (settings.controller_addr === "") {
                ++validate
                _message += "* É necessário indicar um controlador<br/>"
            }
            if (settings.controller_pin === "" || settings.controller_pin === 0) {
                ++validate
                _message += "* É necessário indicar o código PIN do controlador"
            }

            if (validate > 0) {
                object.notification = _message
                return
            }

            tcp_connect.addressController  = settings.controller_addr
            object.notification = qsTr("Tentando estabelecer conexão...")
            tcp_connect.client_connectController()
        }
    }

    function reconfigure()
    {
        DB.destroy()
        DB.init()
        reset_timer();

        settings.type = 0
        settings.pin = ""
        settings.first_use = true
        settings.pin_manual = false
        settings.controller_manual = false
        settings.controller_name = ""
        settings.controller_addr = ""
        settings.controller_pin = 0
        settings.controller_alert = 0
        settings.local_name = ""
        settings.local_addr = ""
        settings.timer_configured_running = false
        settings.timer_configured_time = 0
        settings.timer_configured_time_start = 0
        settings.flag_window = false
        settings.flag_window_fullscreen = false
        settings.oled_display = false

        countdown.prepareStopTime()
        tcp_connect.client_disconnectController()

        list_defined_time.clear()
    }

    Component.onCompleted: {
        platformFlags()

//        if (settings.first_use)
//            console.log("Open First use")

        if (settings.type)
        {
            networkDiscovery.sendSignalToConnection()
            DB.init()
            getListDefinedTime()
            var timer_running = DB.getTimerRunning()
            if (typeof timer_running === 'object' && timer_running.running)
            {
                for (var k = 0; k < list_defined_time.count; ++k)
                {
                    if (list_defined_time.get(k).id === timer_running.discourse_id)
                    {
                        controllerPage.running_timer.id = k
                        countdown.continueTimerConfigured(timer_running)
                        break
                    }
                }
            }
        }
    }

    onClosing: {
        if (Qt.platform.os == "android")
        {
            close.accepted = false
        }
    }
}

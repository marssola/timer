QT += gui quick network qml quick quickcontrols2 sql widgets
# android: QT += androidextras
CONFIG += c++11

#QTPLUGIN+= qsqlite

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    source/hostinfo.h \
    source/networkdiscovery.h \
    source/tcpconnect.h \
    source/countdown.h \
    source/utils/share/share.h \
    source/utils/statusbar/statusbar.h \
    source/utils/statusbar/statusbar_p.h \
    source/utils/keepscreenon/keepscreenon.h

SOURCES += main.cpp \
    source/hostinfo.cpp \
    source/networkdiscovery.cpp \
    source/tcpconnect.cpp \
    source/countdown.cpp \
    source/utils/statusbar/statusbar.cpp

android {
    QT += androidextras
    SOURCES += source/utils/share/share_android.cpp \
               source/utils/statusbar/statusbar_android.cpp \
               source/utils/keepscreenon/keepscreenon_android.cpp

    DISTFILES += \
        android/AndroidManifest.xml \
        android/res/values/libs.xml \
        android/res/drawable-hdpi/icon.png \
        android/res/drawable-ldpi/icon.png \
        android/res/drawable-mdpi/icon.png
    OTHER_FILES += android/src/com/timer/QShareUtils.java
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
} else:ios {
    LIBS += -framework UIKit
    OBJECTIVE_SOURCES += source/utils/share/share_ios.mm \
                         source/utils/statusbar/statusbar_ios.mm \
                         source/utils/keepscreenon/keepscreenon_ios.mm

    Q_ENABLE_BITCODE.name = ENABLE_BITCODE
    Q_ENABLE_BITCODE.value = NO
    QMAKE_MAC_XCODE_SETTINGS += Q_ENABLE_BITCODE

    QMAKE_INFO_PLIST = iOS/Info.plist
    QMAKE_ASSET_CATALOGS += iOS/Images.xcassets
} else {
    SOURCES += source/utils/share/share.cpp \
        source/utils/statusbar/statusbar_dummy.cpp \
        source/utils/keepscreenon/keepscreenon.cpp
}

win32 {
    RC_ICONS += img/Timer.ico
}

darwin {
    ICON = img/Timer.icns
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

VERSION = 1.0.1
DEFINES += GIT_VERSION="\\\"$$system(git --git-dir $$PWD/.git --work-tree $$PWD describe --always --tags)\\\""

#include "share.h"
#include <QGuiApplication>
#include <QClipboard>

Share::Share(QQuickItem *parent) : QQuickItem(parent)
{
}

void Share::share(const QString &content)
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    clipboard->setText(content);
    emit shareContent();
}

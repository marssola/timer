#import "keepscreenon.h"
#import <UIKit/UIKit.h>
#import <QDebug>

KeepScreenOn::KeepScreenOn(QObject *parent) : QObject(parent)
{

}

void KeepScreenOn::on()
{
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
}

void KeepScreenOn::off()
{
    [[UIApplication sharedApplication]setIdleTimerDisabled:NO];
}

#include "hostinfo.h"

HostInfo::HostInfo(QObject *parent) : QObject(parent)
{
    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    for (auto &iface : interfaces) {
        if (iface.name().indexOf("wlan") >= 0 || iface.name().indexOf("wlp") >= 0)
            interface = iface.name();
    }
}

QStringList HostInfo::getAddress()
{
    QNetworkInterface iface = QNetworkInterface::interfaceFromName(interface);
    QStringList list_address;
    for (const auto &address : iface.allAddresses())
    {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
            list_address.append(address.toString());
    }
    return list_address;
}

int HostInfo::getPin()
{
    return m_pin;
}

void HostInfo::generatePin(int time)
{
    qsrand(time);
    m_pin = qrand() % 10000;
    emit pinChanged();
}

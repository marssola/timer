#include "countdown.h"

Countdown::Countdown(QObject *parent) :
    QObject(parent), timer(new QTimer()), realtime(new QTimer())
{
    connect(this, SIGNAL(timeChanged()), this, SLOT(TimerStart()));
    connect(timer, SIGNAL(timeout()), this, SLOT(Timer()));
    //connect(realtime, SIGNAL(timeout()), this, SLOT(realtTime()));
    //realtime->start(15);
}

void Countdown::setTimeString(QJsonObject obj)
{
    busy_changed(true);
    m_time = QDateTime::fromString(QString("1970-01-01 %1 -00").arg(obj.value("time").toString()), Qt::ISODate).toTime_t();
    m_time_start = QDateTime::currentDateTimeUtc().toTime_t();
    m_time_end = m_time + m_time_start;

    m_timer_save.insert("defined", m_time);
    m_timer_save.insert("start", m_time_start);
    m_timer_save.insert("end", m_time_end);
    m_timer_save.insert("discourse_id", obj.value("discourse_id").toInt());
    m_timer_save.insert("running", 1);
    emit saveTimerRunningChanged();

    timer->setInterval(m_interval);
    prepareStartTime();
    busy_changed(false);
}

void Countdown::getCurrentTimeController(QJsonObject obj_time)
{
    m_current_time = QDateTime::currentDateTimeUtc().toTime_t();
    m_current_time_controller = obj_time.value("time").toInt();
    m_diff = m_current_time - m_current_time_controller;
}

void Countdown::setTimeFromController(QJsonObject get_timer)
{
    busy_changed(true);
    m_time = get_timer.value("time").toInt();
    m_time_start = get_timer.value("time_start").toInt();
    m_time_end = get_timer.value("time_end").toInt();
    m_time_alert = get_timer.value("time_alert").toInt();

    settings.setValue("timer_configured_running", true);
    settings.setValue("timer_configured_time", m_time);
    settings.setValue("timer_configured_time_start", m_time_start);

    settings.sync();

    emit timeChanged();
    emit time_alertChanged();
    busy_changed(false);
}

void Countdown::continueTimerConfigured(QJsonObject obj)
{
    busy_changed(true);
    m_time = obj.value("defined").toInt();
    m_time_start = obj.value("start").toInt();
    m_time_end = obj.value("end").toInt();
    m_time_configured_running = obj.value("running").toInt()? true : false;

    if (m_time_configured_running && m_time_start > 0 && m_isController)
    {
        timer->setInterval(m_interval);
        prepareStartTime();
    }
    else
        prepareStopTime();

    busy_changed(false);
}

void Countdown::setTime_alert(qint32 quint_time)
{
    m_time_alert = quint_time;
    emit time_alertChanged();
}

void Countdown::prepareStartTime()
{
    QJsonObject obj;
    m_object_time = QJsonObject{};
    m_object_time.insert("time", (int) m_time);
    m_object_time.insert("time_start", (int) m_time_start);
    m_object_time.insert("time_end", (int) m_time_end);
    m_object_time.insert("time_alert", (int) m_time_alert);
    obj.insert("start", m_object_time);
    m_object_time = obj;

    emit send_timerChanged();
    emit timeChanged();
}

void Countdown::prepareRequestTimerIfRunning()
{
    if (m_timer_pause || !m_status_timer)
        emit send_commandChanged();
    else
        prepareStartTime();
}

void Countdown::setIsController(bool controller)
{
    m_isController = controller;
    emit isControllerChanged();
}

void Countdown::prepareStopTime()
{
    m_object_command = {};
    m_object_command.insert("action", "stop");
    emit send_commandChanged();
    TimerStop();
}

void Countdown::preparePauseTime()
{
    m_object_command = {};
    m_object_command.insert("action", "pause");
    emit send_commandChanged();
    TimerPause();
}

void Countdown::prepareResumeTime()
{
    m_object_command = {};
    m_object_command.insert("action", "resume");
    emit send_commandChanged();
    TimerResume();
}

void Countdown::getCommand(QJsonObject get_command)
{
    if (get_command.value("action") == "stop")
        TimerStop();
    if (get_command.value("action") == "pause")
        TimerPause();
    if (get_command.value("action") == "resume")
        TimerResume();
}

void Countdown::timeToString(qint32 get_time)
{
    m_convert_hours = QDateTime::fromTime_t(get_time).toUTC().toString("hh");
    m_convert_minutes = QDateTime::fromTime_t(get_time).toUTC().toString("mm");
    m_convert_seconds = QDateTime::fromTime_t(get_time).toUTC().toString("ss");

    emit convertHoursChanged();
    emit convertMinutesChanged();
    emit convertSecondsChanged();
}

void Countdown::Timer()
{
    qint32 current_time = QDateTime::currentDateTimeUtc().toTime_t() - m_diff;
    m_timer = current_time - m_time_start;
    qint32 time_left = m_time - m_timer;

    if (time_left < 0 && !m_time_closed)
    {
        m_time_closed = true;
        emit time_closedChanged();
    }

    if (time_left < m_time_alert && !m_alert)
    {
        qint32 alert_time = m_time_end - current_time;
        m_alert = (alert_time > 0)? alert_time * 1000 : 500;
        emit alertChanged();
    }

    emit hoursChanged();
    emit minutesChanged();
    emit secondsChanged();
}

void Countdown::TimerStart()
{
    timer->start();
    m_status_timer = true;
    status_timerChanged();
    keepScreen.on();
}

void Countdown::TimerStop()
{
    timer->stop();
    m_timer = 0;
    m_time = 0;
    m_status_timer = false;
    m_timer_pause = false;
    m_time_closed = false;
    m_alert = 0;

    settings.setValue("timer_configured_running", false);
    settings.setValue("timer_configured_time", m_time);
    settings.setValue("timer_configured_time_start", m_time);
    settings.sync();

    emit alertChanged();
    emit timerPauseChanged();
    emit time_closedChanged();
    emit status_timerChanged();
    emit hoursChanged();
    emit minutesChanged();
    emit secondsChanged();
    keepScreen.off();
}

void Countdown::TimerPause()
{
    timer->stop();
    m_timer_pause = true;
    emit timerPauseChanged();
}

void Countdown::TimerResume()
{
    TimerStart();
    m_timer_pause = false;
    emit timerPauseChanged();
}

void Countdown::realtTime()
{
    m_real_time = QDateTime::currentDateTimeUtc().toString("dd/MM/yyyy hh:mm:ss:zzz");
    emit getRealTimeChanged();
}

void Countdown::busy_changed(bool _busy)
{
    m_busy = _busy;
    emit busyChanged();
}
